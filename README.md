# Installation

```
npm install --save es6-angular-util
```

# Usage

## Controller

The `Controller` class adds three utility methods for $watch, $emit and $on.

```javascript
// foo.controller.js

import {Controller} from 'es6-angular-util'

export default class Foo extends Controller
{
    constructor ($scope)
    {
        super($scope);
    }
    
    bar ()
    {
        this.emit(BAR);
    }
}

export const BAR = 'Foo.BAR'
```

## Directive

The `Directive` class simplifies creating directives by passing the controller and template for assignment in the super class. 

```javascript
// foo.directive.js

import {Directive} from 'es6-angular-util'

import controller from './foo.controller'
import template from './foo.html'

export default class Foo extends Directive
{
    constructor ()
    {
        super(controller, template)
    }
}
```

The third and fourth arguments allow you to specify scope variables and the `controllerAs` value:


```javascript
// foo.directive.js

import {Directive} from 'es6-angular-util'

import controller from './foo.controller'
import template from './foo.html'

export default class Foo extends Directive
{
    constructor ()
    {
        super(controller, template, {bar: '='}, 'baz')
    }
}
```

```html
<!-- foo.html -->
{{baz.bar}}
```

```html
<!-- app.html -->

<foo bar="someBindableValue"></foo>
```