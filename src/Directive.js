export default class Directive
{
    constructor (controller, template, scope, controllerAs)
    {
        this.controller = controller;
        this.template = template;
        this.controllerAs = controllerAs || 'ctl';
        this.bindToController = true;
        this.scope = scope;
    }

    link (scope, element, attributes)
    {
        if (attributes.name != null)
        {
            var parentAs = attributes.parentAs || 'ctl';
            var controllerAs = attributes.controllerAs || 'ctl';

            scope.$parent[parentAs][attributes.name] = scope[controllerAs];
            scope.$emit(SCOPE_ADDED, attributes.name, scope[controllerAs]);
        }
    }
}

export const SCOPE_ADDED = 'Directive.SCOPE_ADDED';