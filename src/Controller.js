export default class Controller
{
    constructor ($scope)
    {
        this.$scope = $scope;
    }

    watch (property, listener)
    {
        this.$scope.$watch(property, listener);
    }

    emit ()
    {
        this.$scope.$emit.apply(this.$scope, arguments);
    }

    on (event, listener)
    {
        this.$scope.$on(event, listener)
    }
}